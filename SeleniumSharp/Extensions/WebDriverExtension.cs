﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace SeleniumSharp.Extensions {
   public static class WebDriverExtension {
        public static IWebElement Search( this IWebDriver driver, string xpath ) {
            IWebElement element = null;
            try {
                element = driver.FindElement( By.XPath( xpath ) );
            } catch ( Exception ) {
                Debug.WriteLine( $"No elements found in xpath: {xpath}" );
            }
            return element;
        }

        public static IReadOnlyCollection<IWebElement> SearchAll( this IWebDriver driver, string xpath ) =>
            driver.FindElements( By.XPath( xpath ) );
    }
}
