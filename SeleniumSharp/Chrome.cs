﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumSharp.ChromeBrowser;
using SeleniumSharp.Extensions;
using SeleniumSharp.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumSharp {

    public class Chrome: IChrome {

        public string CurrentUrl =>
            Driver.Url;

        public IWebDriver Driver { get; private set; }
        public int ProcessId { get; private set; }
        public bool IsRunning { get; private set; }

        private Uri BaseUrl { get; set; }

        public Chrome( string url, bool visual = true, string driverPath = "", params string[ ] arguments ) {
            Task.WaitAll( EnsureChromeDriverIsAvailableAsync( driverPath ) );

            var chromeOptions = new ChromeOptions( );

            var browserArguments = arguments.ToList( );
            browserArguments.Add( "--lang=en-US" );
            browserArguments.Add( "--log-level=3" );
            chromeOptions.AddArguments( browserArguments );

            var service = ChromeDriverService.CreateDefaultService( string.IsNullOrWhiteSpace( driverPath ) ? AppDomain.CurrentDomain.BaseDirectory : driverPath );

            if ( !visual ) {
                chromeOptions.AddArgument( "headless" );
                service.HideCommandPromptWindow = true;
            }

            BaseUrl = new Uri( url );
            Driver = new OpenQA.Selenium.Chrome.ChromeDriver( service, chromeOptions ) { Url = url };

            ProcessId = GetBrowserProcessesId( service.ProcessId );

            Driver
                .Manage( )
                .Timeouts( )
                .ImplicitWait = TimeSpan.FromSeconds( 5 );

            IsRunning = true;
        }

        public static async Task EnsureChromeDriverIsAvailableAsync( string path = "", bool killRemainingDrivers = true ) {
            if ( !ChromeBrowser.ChromeDriver.IsAvailableLocal( path ) )
                await ChromeBrowser.ChromeDriver.DownloadAsync( ChromeLocal.DetectVersion( ), path );

            if ( killRemainingDrivers )
                KillAllOthersDrivers( );
        }

        public static void KillAllOthersDrivers( ) {
            var process = Process.GetProcessesByName( "chromedriver" ).ToList( );
            process.ForEach( x => x.Kill( ) );
        }

        public void Close( ) {
            IsRunning = true;
            ProcessId = 0;
            Driver.Quit( );
        }

        public void Refresh( ) =>
            Driver.Navigate( ).Refresh( );

        public void NavigateTo( string path ) =>
            Driver
                .Navigate( )
                .GoToUrl( BaseUrl.AbsoluteUri + path );

        public IWebElement Search( string xpath ) => Driver.Search( xpath );

        public IReadOnlyCollection<IWebElement> SearchAll( string xpath ) => Driver.SearchAll( xpath );

        public void Wait( int time = 1000 ) => Thread.Sleep( time );

        private int GetBrowserProcessesId( int serviceProcessesId ) {
            var cmd = $"wmic process get processid,parentprocessid,executablepath | findstr \"chrome.exe\" |findstr \"{serviceProcessesId}\"";

            var process = new Process( ) {
                StartInfo = new ProcessStartInfo {
                    FileName = "cmd.exe",
                    Arguments = $"/C \"{cmd}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };

            process.Start( );
            string result = process.StandardOutput.ReadToEnd( );
            process.WaitForExit( );
            process.Close( );

            var value = result.Trim( ).Split( ' ' ).LastOrDefault( );
            return Convert.ToInt32( value );
        }

        public void Dispose( ) => Close( );
    }
}