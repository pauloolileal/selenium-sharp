﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SeleniumSharp.ChromeBrowser {

    public class ChromeDriver {
        private const string _chromeDriverUrl = "https://chromedriver.storage.googleapis.com/";

        public static bool IsAvailableLocal( string path ) =>
            File.Exists( Path.Combine( path, "chromedriver.exe" ) );

        public static async Task DownloadAsync( string version, string path = "" ) {
            var available = await GetLastVersionAvailable( version );

            var client = new HttpClient( ) { BaseAddress = new Uri( _chromeDriverUrl ) };
            var request = await client.GetAsync( $"/{available}/chromedriver_win32.zip" );

            if ( !request.IsSuccessStatusCode )
                throw new Exception( "Not available download for driver!" );

            try {
                var originalFileStream = await request.Content.ReadAsStreamAsync( );

                using ( ZipArchive archive = new ZipArchive( originalFileStream ) ) {
                    foreach ( ZipArchiveEntry entry in archive.Entries ) {
                        entry.ExtractToFile( Path.Combine( path, entry.FullName ), true );
                    }
                }
            } catch ( Exception ) {
                throw new Exception( "Error on extract driver!" );
            }
        }

        public static async Task<string> GetLastVersionAvailable( string chromeVersion ) {
            var chromeBaseVersion = chromeVersion.Split( '.' ).FirstOrDefault( );

            var client = new HttpClient( ) { BaseAddress = new Uri( _chromeDriverUrl ) };
            var request = await client.GetAsync( $"LATEST_RELEASE_{chromeBaseVersion}" );

            if ( !request.IsSuccessStatusCode )
                throw new Exception( "Version not available!" );

            return await request.Content.ReadAsStringAsync( );
        }
    }
}