﻿using Microsoft.Win32;
using System.Diagnostics;

namespace SeleniumSharp.ChromeBrowser {

    public class ChromeLocal {

        public static string DetectVersion( ) {
            var path = Registry.GetValue( @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe", "", null );
            if ( path == null )
                path = Registry.GetValue( @"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe", "", null );

            return FileVersionInfo.GetVersionInfo( path.ToString( ) ).FileVersion;
        }
    }
}