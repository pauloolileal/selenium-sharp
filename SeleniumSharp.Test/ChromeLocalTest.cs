using SeleniumSharp.ChromeBrowser;
using Xunit;

namespace SeleniumSharp.Test {

    public class ChromeLocalTest {

        [Fact]
        public void DetectVersion_must_return_value( ) {
            var version = ChromeLocal.DetectVersion( );
            Assert.True( !string.IsNullOrEmpty( version ) );
        }
    }
}